/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package PartiallyMappedCrossover;


import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Permite la lectura de argumentos pasados por linea de comandos o por lectura de
 * archivo. Además permite buscar y recuperar cadenas. NOTA: Desafortunadamente,
 * al parecer no es posible agregar cadenas a la lista ya construida.
 * @author RENE
 */

public class MyListString {
    List <String> WordList = null;
/**
 * Construye un Lista de cadenas a partir de un arreglo de cadenas
 * @param Strings Arreglo de cadenas
 */
MyListString(String []Strings){
 WordList = Arrays.asList(Strings);
 this.ToUpperCase();
}
/**
 * Lee los parámetrso que se pasaron mediante un archivo
 * @param FileName Nombre del archivo con los parámetros
 * @return TRUE: si se pudo cargar el archivo
 */
boolean AddArgsFromFile (String FileName ){
 ManejoArchivos IO = new ManejoArchivos();
 String Texto      = IO.Reads_Text_File(FileName);
 String []StrArray = Texto.split("\\s+");

 String []NewParam = new String[WordList.size()+StrArray.length];

 int i;
 for ( i = 0; i < WordList.size(); i++)
    NewParam[i]=WordList.get(i);

 for (int j = 0; j < StrArray.length; j++, i++)
    NewParam[i]=StrArray[j];

 WordList = Arrays.asList(NewParam);
 this.ToUpperCase();

return (true);
}

/**
 * Convierte cada una de las cadenas a mayúsculas
 */
final void ToUpperCase(){
  for (int i=0; i<WordList.size();i++){
     WordList.set(i, WordList.get(i).toUpperCase());
     }
}
/**
 * Convierte cada una de las cadenas a minúsculas
 */
void ToLowerCase(){
  for (int i=0; i<WordList.size();i++){
     WordList.set(i, WordList.get(i).toLowerCase());
     }
}
/**
 * Busca si una cadena Word está en el arreglo de cadenas. Si no se encuentra regresa 0
 * @param Word Cadena para buscar
 * @return índice de la cadena empezando desde 1
 */
int IndexOf (String Word){
 return (WordList.indexOf(Word)+1);
}
/**
 * Obtiene la cadena que está guardada en la posición índex del arreglo
 * @param index índice de la cadena asociada
 * @return Regresa la cadena asociada al elemento index
 */
String Item (int index){
return (WordList.get(index-1));
}
/**
 * Revisa si existe una cadena en el arreglo
 * @param Label cadena que se va a buscar si existe en el arreglo
 * @return true si existe la cadena Label en el arreglo, falso en otro caso
 */
boolean Exists( String Label){
int temp=-1;
return( (temp = IndexOf(Label)) > 0 ? true : false );
}
/**
 * Se utiliza para leer parámetros de tipo String, primero se busca la cadena en la lista de cadenas y se regresa la siguiente cadena como STRING
 * @param Label párametro a leer
 * @param Default Valor por default que se regresa si no se encuentra Label en el arreglo
 * @return el valor del parámetro como una cadena
 */
String ValueArgsAsString( String Label , String Default){
int temp=-1;
return( (temp = IndexOf(Label)) > 0 ? Item(temp+1) : Default );
}
/**
 * Se utiliza para leer parámetros de tipo int, primero se busca la cadena
 * en la lista de cadenas y se regresa la siguiente cadena como int
 * @param Label párametro a leer
 * @param Default Valor por default que se regresa si no se encuentra Label en el arreglo
 * @return el valor del parámetro como un entero (int)
 */
int ValueArgsAsInteger( String Label, int Default ){
int temp=-1;
return( (temp = IndexOf(Label)) > 0 ? Integer.valueOf(Item(temp+1)) : Default );
}
/**
 * Se utiliza para leer parámetros de tipo float, primero se busca la cadena
 * en la lista de cadenas y se regresa la siguiente cadena como float
 * @param Label párametro a leer
 * @param Default Valor por default que se regresa si no se encuentra Label en el arreglo
 * @return el valor del parámetro como un real (float)
 */
float ValueArgsAsFloat( String Label, float Default ){
int temp=-1;
return( (temp = IndexOf(Label)) > 0 ? Float.valueOf(Item(temp+1)) : Default );
}
/**
 * Se utiliza para leer parámetros de tipo boolean, primero se busca la cadena
 * en la lista de cadenas y se regresa la siguiente cadena como boolean
 * @param Label párametro a leer
 * @param Default Valor por default que se regresa si no se encuentra Label en el arreglo
 * @return el valor del parámetro como un boolean
 */
boolean ValueArgsAsBoolean( String Label, boolean Default ){
int temp=-1;
return( (temp = IndexOf(Label)) > 0 ? Item(temp+1).equalsIgnoreCase("true") : Default );
}

/**
 * Se utiliza para leer parámetros de tipo Calendar, primero se busca la cadena
 * en la lista de cadenas y se regresa la siguiente cadena como Calendar. Puesto
 * que en nuestro sistema el mes 1 corresponde a ENERO entonces a nivel de
 * codificación se le resta 1 al mes para coincidir con la fecha de Calendar.
 * @param Label parámetro a leer
 * @param Default Valor por defaul en caso de no encontrarse la fecha
 * @return la fecha en formato Calendar, en caso de no hacer regresa null
 */
Calendar ValueArgsAsDate ( String Label, String Default ){
Calendar Fecha  = null;
ManejoER Format = new ManejoER("(\\d{2,4})/(\\d\\d)/(\\d\\d)/(\\d\\d)/(\\d\\d)");
int temp=-1;
String Date = (temp = IndexOf(Label)) > 0 ? Item(temp+1) : Default ;

if ( Format.ExistER(Date) ){
     Fecha = Calendar.getInstance();
     Fecha.set(Integer.valueOf(Format.Grupo(1)), Integer.valueOf(Format.Grupo(2))-1,
               Integer.valueOf(Format.Grupo(3)), Integer.valueOf(Format.Grupo(4)),
               Integer.valueOf(Format.Grupo(5)));
    }

// System.out.println( Fecha.get(Calendar.YEAR)+"/"+ Fecha.get(Calendar.MONTH)+"/"+
//                     Fecha.get(Calendar.DAY_OF_MONTH)+"/"+Fecha.get(Calendar.HOUR_OF_DAY)+"/"+
//                     Fecha.get(Calendar.MINUTE));
return(Fecha);
}

/**
 * Se utiliza para chechar la sintaxis revisando primero la disyuncion (OR), es 
 * decir que por lo menos se cumpla para una celda del arreglo, pero si una celda
 * del arreglo tiene a su vez varias etiquetas (separadas por espacio en blanco)
 * entonces deben de cumplirse todas las etiquetas para que esa celda sea 
 * considerada satisfecha (verdadera) y para que OR se considere también satisfecho
 * , aún cuando ya se haya cumplido alguna otra celda
 * @param Lables Arreglo con las etiquetas a satisfacer
 * @return TRUE: si por lo menos alguna de las celdas se satisfacio
 */
boolean SintaxisOrAnd (String []Labels){

for (String CeldaOR: Labels){ //PRIMERO: En este FOR se van a revisar las celdas que contienen varias etiquetas
    String []Chk = CeldaOR.trim().split("\\s+");
    if (Chk.length>1){
       boolean flag=true;
       boolean Need=false;
       for (String CeldaAND : Chk){
          if (this.IndexOf(CeldaAND.toUpperCase().trim()) > 0){ 
                 flag = flag&true;
                 Need = true;
             }//del if
          else
                 flag = flag&false;
          }//del for
       if (Need & !flag)
           return (false); //Una de las celdas que tiene varias etiquetas, al menos existia una etiqueta y otra no
       else if (Need & flag)
           return(true);
       }

    }//del for

for (String CeldaOR: Labels){ //SEGUNDO: En este FOR se van a revisar las celdas que solo tiene una etiqueta
    String []Chk = CeldaOR.trim().split("\\s+");
    if (Chk.length==1){
       if (this.IndexOf(Chk[0].toUpperCase().trim()) > 0){ //con una que se cumpla regresa true
          return(true);
          }//del for
       }
    }//del for

 return (false);
}

/**
 * Se utiliza para chechar la sintaxis revisando si existen todas las etiquetas
 * que aparecen en la línea Params, con una que no aparezca regresa false
 * @param Params etiquetas que deben de existir
 * @return TRUE: si todas las etiquetas de Params existen
 */
boolean SintaxisAND (String Params){
String []Chk = Params.trim().split("\\s+");
for (String Etiqueta :Chk) {
    if (this.IndexOf(Etiqueta.toUpperCase().trim()) == 0)//entonces no se encontró
        return (false);
}//del for
return (true);
}

/**
 * Se utiliza para chechar la sintaxis revisando primero que si existe alguna de
 * las etiquetas de Trigger entonces deben de aparecer todas las etiquetas del
 * arreglo MUST, en caso de que Must contenga un celda por varias etiquetas entonces
 * deben de cumplirse todas para que esa celda sea considerada satisfecha
 * @param Trigger Arreglo con las etiquetas IF si alguna se cumple todas las de
 *        Must deben de cumplirse
 * @param Must Todas las etiquetas que deben cumplirse si alguna de Trigger se cumple
 * @return TRUE: si se cumple, en caso de que no se cumpla ninguna etiqueta de
 * Trigger entonces se regresa TRUE.
 */
boolean SintaxisIfSomeThenAll (String []Trigger, String []Must){
boolean IF = false;
for (String CeldaIF: Trigger){
    if (this.SintaxisAND(CeldaIF)){
      IF = true;
      break;
      }
  }//del for

if (!IF) return (true);

for (String CeldaTHEN: Must){
    if (!this.SintaxisAND(CeldaTHEN)){
      return (false);
      }
  }//del for
return (true);
}


}// fin de MyListString

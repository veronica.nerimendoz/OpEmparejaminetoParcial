/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PartiallyMappedCrossover;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author UAPT
 */
public class threadPMX1 implements Runnable
{
    Map<Integer, Integer> MPA = new HashMap<>();
    Map<Integer, Integer> MPB = new HashMap<>();
    Map<Integer, Integer> HijoA = new HashMap<>();
    Map<Integer, Integer> Hijo_A = new HashMap<>();
    
    ManejoArchivos IO;
    IOBinFile      BinFile;
    
    float [] PA;
    float [] PB;
    String FNEXTGPOP;
    String NOMSON;
    
    
    public threadPMX1(float [] pa, float [] pb,String fnextgpop, String nomson)
    {
        this.PA=pa;
        this.PB=pb;
        this.FNEXTGPOP=fnextgpop;
        this.NOMSON=nomson;
    }
    
    @Override
    public void run() 
    {
        ArrayList<Integer> SeleccionPuntosCorte = new ArrayList<>();
        int Punto1=0;
        int Punto2=0;

        //Carga los hijos array en los mapas hijos
        for (int j = 0; j < PA.length; j++) 
        {
            MPA.put(j, (int) PA[j] );
            SeleccionPuntosCorte.add(j);
        }

        for (int j = 0; j < PB.length; j++) 
        {
            MPB.put(j, (int) PB[j] );
        }
        //**********************Se realiza la cruza********************/**** 
        //Se extraen los dos puntos a leatorios
        Collections.shuffle(SeleccionPuntosCorte);
        if (SeleccionPuntosCorte.get(0)<SeleccionPuntosCorte.get(1)) 
        {
            Punto1=SeleccionPuntosCorte.get(0);
            Punto2=SeleccionPuntosCorte.get(1);
        }
        else
        {
            Punto1=SeleccionPuntosCorte.get(1);
            Punto2=SeleccionPuntosCorte.get(0);
        }

        //Asignamos los valores de los puntos de corte a los hijos
        int auxA=Punto1;
        while(auxA<=Punto2)
        {
            HijoA.put(auxA, MPB.get(auxA));

            auxA++;
        }
        int auxB=Punto1;
        while(auxB<=Punto2)
        {
            HijoA.put(auxB, MPB.get(auxB));

            auxB++;
        }

        int Insercion=0;
        while(Insercion<MPA.size())
        {
            InsercionGenHA(Insercion);
            Insercion++;
        }

        //Insertamos valores que estan repetidos 
        GeneticInsertion giHA = new GeneticInsertion(MPA,MPB,HijoA);
        Hijo_A = giHA.RUNPMX();

        for (int j = 0; j < this.Hijo_A.size(); j++) 
        {
            PA[j]=(float)this.Hijo_A.get(j); 
        }

        //*************************Fin de la cruza**************************
        BinFile.WriteBinFloatFileIEEE754(IO.AddToPath(FNEXTGPOP, NOMSON), PA);

    }
    
    //Realiza la insercion del Gen del padre PA al hijo HA
    public boolean InsercionGenHA(int indkey)
    {
        boolean Insercion = false;
        if (!this.HijoA.containsValue(this.MPA.get(indkey)) && !this.HijoA.containsKey(indkey)) 
        {
            this.HijoA.put(indkey, this.MPA.get(indkey));
            Insercion = true;
        }
        return Insercion;
    }
}

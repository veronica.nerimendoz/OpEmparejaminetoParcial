/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PartiallyMappedCrossover;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author UAPT
 */
public final class GeneticInsertion 
{
    Map<Integer, Integer> PadreA = new HashMap<>();
    Map<Integer, Integer> PadreB = new HashMap<>();
    Map<Integer, Integer> Hijo = new HashMap<>();
    
    GeneticInsertion(Map<Integer, Integer> PA,Map<Integer, Integer> PB,Map<Integer, Integer> HA)
    {
        this.PadreA = PA;
        this.PadreB = PB;
        this.Hijo   = HA;
    }

    public Map<Integer, Integer> RUNPMX()
    {
        //Insertamos valores que estan repetidos 
        int Insercion1=0;
        int IndicePB=0;
        int IndicePA=0;
        boolean InsercionVR = true;//Insercion en valores que estan repetidos
        while(Insercion1 < PadreA.size() && Hijo.size() != PadreA.size())
        {
            if (!this.Hijo.containsKey(Insercion1))
            {
                //1.- Determinar si el valor del padre PA a partir de su key esta contenido en HA
                //2.- Si el valor esta contenido en el HA
                //2.1.-Optener el indice del PB apartir del valor del PA
                //2.2.-Hasta la insercion del valor 
                //1.- ***Linea de segimiento***
                //System.out.println("keyPA: "+Insercion1+" ValorPA_HA: "+ValorPA_esta_HA(Insercion1));
                if (ValorPA_esta_HA(Insercion1)) 
                {
                    //Inicializacion de la key de PA
                    IndicePA=Insercion1; 
                    //2.- ***Linea de segimiento***
                    //System.out.println("keyPA: "+IndicePA);
                    while(InsercionVR && IndicePA<this.PadreA.size())
                    {
                        IndicePB= keyPB(PadreA.get(IndicePA));
                        if (!ValorPA_esta_HA(IndicePB)) 
                        {
                            InsercionGenRepetida(keyHA_VPA(),IndicePB);
                            InsercionVR=false;
                        }
                        IndicePA=IndicePB;
                    }
                    InsercionVR=true;
                }
            }
            Insercion1++;
        }
        return this.Hijo;
    }
    
     
    //Identifica el valor de PA a partir de su key
    public boolean ValorPA_esta_HA(int keyPA)
    {
        boolean valorPA_HA= false;
        if (Hijo.containsValue(PadreA.get(keyPA))) 
        {
            valorPA_HA = true;
        }
        return valorPA_HA;
    }
    
    //Identifica la llave del padre PB apartir del valor del padre PA
    public int keyPB(int valorPA)
    {
        int Key=0;
        while(valorPA!=(PadreB.get(Key)))
        {
            Key++;
        }
        return Key;
    }

    //Identifica el valor del padre PA a partir de la key del padre PB
    public int ValorPA(int keyPB)
    {
        int valor=0;
        valor = this.PadreA.get(keyPB);
        return valor;
    }
    
    //Realiza la insercion del Gen del padre PA al hijo HA
    public boolean InsercionGen(int indkey)
    {
        boolean Insercion = false;
        if (!Hijo.containsValue(PadreA.get(indkey)) && !this.Hijo.containsKey(indkey)) 
        {
            Hijo.put(indkey, PadreA.get(indkey));
            Insercion = true;
        }
        return Insercion;
    }
    
    //Realiza la insercion del Gen del padre PA al hijo HA
    public boolean InsercionGenRepetida(int indHijo, int indkey)
    {
        boolean Insercion = false;
        if (!Hijo.containsValue(PadreA.get(indkey))) 
        {
            Hijo.put(indHijo, PadreA.get(indkey));
            Insercion = true;
        }
        return Insercion;
    }
    
    //Identificar el keyHA donde se va insertar el Valor de PA
    public int keyHA_VPA()
    {
        int keyHA=0;
        //Mientras HA seamenor que el tamaño identificamos la primera key a insertar 
        //identificar la key a insertar 
        while(keyHA<this.PadreA.size())
        {
            if (Hijo.get(keyHA)==null) 
            {
                break;
            }
            keyHA++;
        }
        return keyHA;
    }
}

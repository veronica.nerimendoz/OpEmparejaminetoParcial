/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PartiallyMappedCrossover;

/**
 *
 * @author Veronica & Brito
 * @version 1.0
 */
public class Argumentos 
{
     /**-CONFIG Nombre del archivo con los demás parámetros dados abajo*/
    String   ConfigFile;
    /**-FWORK SubRuta donde se van a crear las generaciones */
    String   FWORK;
    /**-FACTG SubRuta (prefijo) de la generación actual, sin incluir paréntesis*/
    String   FACTG;
    /**-FNEXTG  Ruta donde está la siguiente generación*/
    String   FNEXTG;
    /**-FPOP  Ruta donde está la población, ya sea de la generacion actual o de la siguiente generacion*/
    String   FPOP;
    /**-FSELECT  Nombre del archivo (incluyendno extención) donde se van a guardar la selección*/
    String   FSELECT;
    /**-NDIED Número de individuos que van a morir en esta generación,
    * por lo tanto se van a seleccionar NDIED*2 padres */
    int      NDIED;
   /**-XPROB Probabilidad de cruce para cada gen */
    int      XPROB;

    boolean THREADSCRUZA;
            
    int NCHILD;
    
    MyListString   Param;
    StringBuffer   Report;
 
    public Argumentos(String [] Args)
    {
        //*****lectura de argumentos*******
        Param    = new MyListString(Args);

        ConfigFile=  Param.ValueArgsAsString  ( "-CONFIG"     , ""  );
        if (!ConfigFile.equals(""))
            Param.AddArgsFromFile(ConfigFile);

        FWORK    =  Param.ValueArgsAsString  ( "-FWORK"  , "" );
        FACTG    =  Param.ValueArgsAsString  ( "-FACTG"  , "" );
        FNEXTG   =  Param.ValueArgsAsString  ( "-FNEXTG" , "" );
        FPOP     =  Param.ValueArgsAsString  ( "-FPOP"   , "" );
        FSELECT  =  Param.ValueArgsAsString  ( "-FSELECT", "" );
        NDIED    =  Param.ValueArgsAsInteger ( "-NDIED"  , 0  );
        THREADSCRUZA = Param.ValueArgsAsBoolean("-THREADSCRUZA", true );
        NCHILD   = Param.ValueArgsAsInteger ( "-NCHILD"  , 1 );
        String TodosJuntos = "-FWORK -FACTG -FNEXTG -FPOP -FSELECT -NDIED -THREADSCRUZA -NCHILD";

        if (!Param.SintaxisAND(TodosJuntos))
        {
            System.out.println("Error de sintaxis...");
            System.exit(0);
        }
        else
        {
            PMX pmx = new PMX(FWORK,FACTG,FNEXTG,FPOP,FSELECT,NDIED,THREADSCRUZA,NCHILD);
            pmx.RUN();
        }
    }
}

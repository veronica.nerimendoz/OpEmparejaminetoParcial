/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PartiallyMappedCrossover;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Veronica & Brito
 * @version 1.0
 */
public class PMX 
{
    /**-FWORK SubRuta donde se van a crear las generaciones */
    String   FWORK;
    /**-FACTG SubRuta (prefijo) de la generación actual, sin incluir paréntesis*/
    String   FACTG;
    /**-FNEXTG  Ruta donde está la siguiente generación*/
    String   FNEXTG;
    /**-FPOP  Ruta donde está la población, ya sea de la generacion actual o de la siguiente generacion*/
    String   FPOP;
    /**-FSELECT  Nombre del archivo (incluyendno extención) donde se van a guardar la selección*/
    String   FSELECT;
    /**-NDIED Número de individuos que van a morir en esta generación,
    * por lo tanto se van a seleccionar NDIED*2 padres */
    int      NDIED;
    
    boolean THREADSCRUZA;
            
    int NCHILD;
    
    Map<Integer, Integer> MPA = new HashMap<>();
    Map<Integer, Integer> MPB = new HashMap<>();
    Map<Integer, Integer> HijoA = new HashMap<>();
    Map<Integer, Integer> HijoB = new HashMap<>();
    Map<Integer, Integer> Hijo_A = new HashMap<>();
    Map<Integer, Integer> Hijo_B = new HashMap<>();
        
    ManejoArchivos IO;
    IOBinFile      BinFile;
    
    public PMX(String FWORK, String FACTG, String FNEXTG, String FPOP, String FSELECT, int NDIED,boolean threads,int nchild)
    {
        this.FWORK   = FWORK;
        this.FACTG   = FACTG;
        this.FNEXTG  = FNEXTG;
        this.FPOP    = FPOP;
        this.FSELECT = FSELECT;
        this.NDIED   = NDIED;
        this.THREADSCRUZA=threads;
        this.NCHILD=nchild;
    }
    
    public void RUN()
    {
        String [] Folders   = IO.List_Carpetas(FWORK);
        String    FACTGEN   = "";
        String    FACTGPOP  = "";
        String    FNEXTGPOP = "";

        for (String string : Folders) 
        {
            if (string.toUpperCase().startsWith(FACTG))
            {
                FACTGEN  = IO.AddToPath(FWORK, string);
                FACTGPOP = IO.AddToPath(FACTGEN,FPOP);
                break;
            }
        }
        for (String string : Folders) 
        {
            if (string.toUpperCase().startsWith(FNEXTG))
            {
                FNEXTGPOP  = IO.AddToPath(IO.AddToPath(FWORK, string),FPOP);
                break;
            }
        }

        ManejoER  ER   = new ManejoER("([a-zA-Z]*)(\\d+)([a-zA-Z]*)\\.bin");
        String [] Inds = IO.List_Files(FACTGPOP, ".bin");
        String    Pre  = "";
        int       Inf  = 0;
        String    Post = "";
        int       Max  = 0;
        //Obtener la sintaxis de los nombres de archivos, prefijo, infijo y postfijo.
        //En el infijo viene el número secuencial, el cual se a utlizar para generar a los hijos
        for (int i=0; i<Inds.length ;i++)
        {
            if (ER.ExistER(Inds[i]))
            {
                Pre = ER.Grupo(1);
                Inf = Integer.valueOf( ER.Grupo(2));
                Post= ER.Grupo(3);
            }
             if (i==0)    Max = Inf;
             if (Inf>Max) Max = Inf;
        }//del for

        String [] Parents = IO.Read_Text_File(IO.AddToPath(FACTGEN,FSELECT));
        for (int i = 0; i < Parents.length; i++)  //se le agrega a cada archivo la ruta completa
             Parents[i] = IO.AddToPath(FACTGPOP, Parents[i]);

        float  [] PA      = (Parents.length>0)? BinFile.ReadBinFloatFileIEEE754(Parents[0]):null; //Padre A
        float  [] PB      = null; //Padre B

        //Threads
        int thread=0;
        if (THREADSCRUZA) 
        {
            Runtime runTime = Runtime.getRuntime();
            thread = runTime.availableProcessors();
        }
        else
        {
            thread=1;
        }

        switch (thread) 
        {
            case 1:  
                for (int i = 1, p=0 ; i <= NDIED; )//Se realiza la cruza con un hilo  
                {
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    executorH.execute(H1);
                    executorH.shutdown(); 
                }
                     break;
            case 2:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*2)//Se realiza la cruza con un hilo  
                {
                    //Carga los hijos
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if (p<Parents.length- 1) 
                    {
                         PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                         PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                         threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                         executorH.execute(H2);
                    }
                    
                    executorH.shutdown(); 
                }
                     break;
            case 3:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*3)//Se realiza la cruza con un hilo  
                {
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H2);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H3 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H3);
                    }

                    executorH.shutdown(); 
                }
                     break;
            case 4:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*4)//Se realiza la cruza con un hilo  
                {
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H2);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H3 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H3);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H4 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H4);
                    }
                    executorH.shutdown(); 
                }
                     break;
            case 5:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*5)//Se realiza la cruza con un hilo  
                {
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H2);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H3 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H3);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H4 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H4);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H5 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H5);
                    }
                    executorH.shutdown(); 
                }
                     break;
            case 6:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*6)//Se realiza la cruza con un hilo  
                {
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H2);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H3 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H3);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H4 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H4);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H5 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H5);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H6 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H6);
                    }
                    executorH.shutdown(); 
                }
                     break;
            case 7:  
                for (int i = 1, p=0 ; i <= NDIED; i=i*7)//Se realiza la cruza con un hilo  
                {
                    ExecutorService executorH = Executors.newCachedThreadPool();
                    //Carga los hijos 
                    PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                    threadPMX1 H1 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                    executorH.execute(H1);
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H2 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H2);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H3 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H3);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H4 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H4);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H5 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H5);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H6 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H6);
                    }
                    if(p<Parents.length- 1)
                    {
                        PA     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        PB     = BinFile.ReadBinFloatFileIEEE754(Parents[p++]);
                        threadPMX1 H7 = new threadPMX1(PA,PB,FNEXTGPOP,Pre+(++Max)+Post+".bin");
                        executorH.execute(H7);
                    }
                    executorH.shutdown(); 
                }
                     break;
            default: System.out.println("Invalid the number of threads");
                     break;
        }
    }
}
